#!/usr/bin/python3
"""
This is a WIP for grabbing owners of debian.net subdomains in order to feed
debian.net contributors.d.o. This should work by:
 - getting active services from https://prometheus.debian.net
 - getting its owners from ldap://db.debian.org
"""

import requests
import subprocess
from requests.auth import HTTPBasicAuth
import re

base_url = 'https://prometheus.debian.net/'

response = requests.get(base_url + '/api/v1/query',
                        auth=HTTPBasicAuth('debian.net', 'debian.net'),
                        params={
                            'query': 'avg_over_time(probe_success{job="icmp"}[7d]) > 0.8',
                            })

results = response.json()['data']['result']

def parse_ldap_entries(output: str) -> list[str]:
    r = re.compile(r"uid: (.+)\n", re.MULTILINE)
    return r.findall(output)

#parse_ldap_entries("""LDAPv3
## base <dc=debian,dc=org> with scope subtree
## filter: (dnsZoneEntry=dcf *)
## requesting: uid
##
#
## tiago, users, debian.org
#dn: uid=tiago,ou=users,dc=debian,dc=org
#ufn: tiago, users, debian.org
#uid: tiago
#
## search result
#search: 2
#result: 0 Success
#
## numResponses: 2
## numEntries: 1
#""")

uids_lst = []

for result in results:
  # find contributor for each active service with:
  subdomain = result['metric']['instance'].split('.')[0]
  search_str = '(dnsZoneEntry=' + subdomain + ' *)'
  #ldapsearch -u -x -H ldap://db.debian.org -b dc=debian,dc=org '(dnsZoneEntry=dcf *)' uid |grep -oP "uid: \K\w*"
  o = subprocess.check_output(['ldapsearch', '-u', '-x', '-H', 'ldap://db.debian.org', '-b', 'dc=debian,dc=org', search_str, 'uid'], encoding='utf-8')
  uid = parse_ldap_entries(o)
  print(search_str, uid)
  if uid:
    uids_lst.append(uid[0])

contributors = set(uids_lst)

print(contributors)
