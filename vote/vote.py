import requests
import debiancontributors as dc
from datetime import date

vote_date = {
    "2002" : [date(2002,4,16)],
    "2003" : [date(2003,3,30),date(2003,6,20),date(2003,10,29)],
    "2004" : [date(2004,4,11),date(2004,3,21),date(2004,4,25),date(2004,7,2)],
    "2005" : [date(2005,4,11),date(2005,12,31)],
    "2006" :
[date(2006,3,12),date(2006,4,9),date(2006,9,24),date(2006,10,15),date(2006,10,15),date(2006,10,15),date(2006,10,15)],
    "2007" : [date(2007,4,8),date(2007,3,18),date(2007,8,5),date(2007,10,7)],
    "2008" : [date(2008,4,13),date(2008,12,14),date(2008,12,27)],
    "2009" : [date(2009,4,11)],
    "2010" : [date(2010,4,15),date(2010,10,18)],
    "2011" : [date(2011,4,15)],
    "2012" : [date(2012,4,14),date(2012,6,2)],
    "2013" : [date(2013,4,13)],
    "2014" : [date(2014,4,13),date(2014,4,27),date(2014,11,18),date(2015,1,8)],
    "2015" : [date(2015,4,14),date(2015,9,9),date(2015,12,12)],
    "2016" : [date(2016,4,16),date(2016,8,20),date(2016,8,13),date(2016,10,22)],
    "2017" : [date(2017,4,15)],
    "2018" : [date(2018,4,16)],
    "2019" : [date(2019,4,20),date(2019,12,27)],
    "2020" : [date(2020,4,18)],
    "2021" : [date(2021,4,17),date(2021,4,17),date(2022,1,28)],
    "2022" : [date(2022,3,26),date(2022,4,16),date(2022,10,1)],
    "2023" : [date(2023,4,14)]
}

vote_url = "https://www.debian.org/vote/"

s = dc.Submission("vote.debian.org")
#s.auth_token = "foobar"
s.auth_token = "Vr7HU0tMg60gd0AE"

for year in vote_date.keys():
    print("Processing votes from " + year + "...")

    for vote_number in range(1, len(vote_date[year]) + 1):
        url = (vote_url + year + "/vote_00" + str(vote_number))
        date = vote_date[year][vote_number-1]

        # dealing with voters url change
        if int(year) == 2002:
            voters_url = "https://www.debian.org/vote/2002/voters.txt"

        if int(year) == 2003:
            if vote_number == 1:
                voters_url = "https://www.debian.org/vote/2003/leader2003_voters.txt"
            if vote_number == 2:
                voters_url = "https://www.debian.org/vote/2003/gr_voting_voters.txt"
            if vote_number == 3:
                voters_url = "https://www.debian.org/vote/2003/gr_sec415_voters.txt"

        if int(year) == 2004 or int(year) == 2005:
            voters_url = url + ".quorum.log"

        if int(year) > 2005 :
            voters_url = url + "_quorum.log"

        response = requests.get(voters_url)

        if not response:
            print("No quorum log for "+ year + "/vote_00" + str(vote_number))
        else:
            lines = response.text.split("\n")

            for line in lines:
                words = line.split("\t")

                # dealing with format change in voters log
                if int(year) < 2020:
                    login_index = 1
                else:
                    login_index = 2

                if int(year) < 2015:
                    if len(words) > 0:
                        inner_words = words[0].split()
                        if len(inner_words) > 1:
                            login = inner_words[1].strip()
                            if login != "Debian":
                                s.add_contribution(
                                    dc.Identifier("login", login),
                                    dc.Contribution("vote", date, date, vote_url))                            
                else:
                    if len(words) >= 3:
                        s.add_contribution(
                            dc.Identifier("login", words[login_index].strip()),
                            dc.Contribution("vote", date, date, vote_url))

f = open("vote_contributions.txt","w")
j = open("vote_contributions.json","w")
s.print_compact(f)
s.to_json(j)

print("   Post contributions: ", end="")
success, info = s.post()
if success:
    print("done!")
else:
    import json
    print("submission failed:")
    print(json.dumps(info, indent=1))
