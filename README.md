## Debian contributor sources QA

DC Sources with last submission more than 1 year ago (as of July 2023) are listed
in the tables below.

The following are to be updated as soon as possible. Contact admins, offer help,
or take the job on our hands.

| Name                   | Admins                       | Status|
|---			 | ---				|---|
| vote			 | enrico kroeckx tassia	| up-to-date|
| Distro Tracker  	 | tobald@debian hertzog	| up-to-date (could go under QA?)|
| www		       	 | neilm larjona tiago tassia	| up-to-date|
| Contrib. data source   | tiago tassia	                | up-to-date|
| Debian Security Tracker| luciano			| up-to-date|
| Perl packaging group 	 | tina				| work-in-progress - missing 'modules', missing url for 'interpreter/perl'|
| DSA 			 | zobel pabs			| to-be-confirmed: [most active repos](https://salsa.debian.org/dsa-team/mirror?sort=latest_activity_desc)|
| publicity 		 | madamezou ana donald larjona | to-be-confirmed: announcements, bits, debian-timeline, DPN, micronews (get repos from [publicity-team](https://salsa.debian.org/publicity-team/)). pinged team in jun 2024, waiting for input (tvaz). Notes: "gitlogs method via dc-tool on alioth.debian.org:/srv/home/groups/publicity/contributors.debian.org"|
| qa.debian.org 	 | zack tina			| to-be-confirmed: openQA, bls, debsources, dose, jenkins, pts, qa, udd | 
| Go packaging group 	 | tiago tina			| to-be-confirmed: [golang](https://salsa.debian.org/go-team/compiler/golang),[golang-defaults](https://salsa.debian.org/go-team/compiler/golang-defaults),[pkg-go-tools](https://salsa.debian.org/go-team/infra/pkg-go-tools),[provisioning](https://salsa.debian.org/go-team/infra/provisioning),[go-team.pages.debian.net](https://salsa.debian.org/go-team/go-team.pages.debian.net),[migrate-pkg-go-to-salsa](https://salsa.debian.org/go-team/migrate-pkg-go-to-salsa)|
| salsa.debian.org 	 | formorer tina		| to-be-confirmed: [salsa-ansible](https://salsa.debian.org/salsa/salsa-ansible), [gitlab salsa edition](https://salsa.debian.org/salsa/gitlab) and [salsa-scripts](https://salsa.debian.org/salsa/salsa-scripts)
| Debian Outreach Team 	 | andrewsh mollydb 		| manual collection, pending| 
| debconf subtitle team  | madamezou tvincent		| discontinued? might return?|
| debconf 		 | chrysn-guest@alioth joerg 	| no submissions ever|

To be kept for historical purposes, but flag that service is discotinnued

| Name                   | Admins                       | Status|
|---			 | ---				|---|
| svn.debian.org 	 | tina				| service discontinued|
| bzr.debian.org 	 | asb				| service discontinued|
| collab-maint 		 | --- 				| service discontinued|

To be removed?

| Name			 | Admins 			| Status|
|---			 | ---				|---|
| salsa-test 		 | formorer tina		| did not pass test|
